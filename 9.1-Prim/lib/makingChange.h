//
// Created by Louis-Mathieu on 2018-05-05.
//

#ifndef INC_9_1_PRIM_MAKINGCHANGE_H
#define INC_9_1_PRIM_MAKINGCHANGE_H

#include <vector>
/**
 * @brief Gives the change for ammount n with the denominations in the vector d
 * using the greedy (non-complete) method.
 * @param n The amount we want the change for.
 * @param d The denominations ordered descendingly.
 * @return The amounts for each denominations, in the same order as d if the change can be made.
 * Empty vector if the change cannot be made using the greedy method
 */
std::vector<int> makeChange(int n, std::vector<int> d);

#endif //INC_9_1_PRIM_MAKINGCHANGE_H
