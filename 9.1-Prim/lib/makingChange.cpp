//
// Created by Louis-Mathieu on 2018-05-05.
//
#include "makingChange.h"

std::vector<int> makeChange(int n, std::vector<int> d){
    std::vector<int> amounts(d.size());
    auto it = amounts.begin();
    int r = n;
    for (auto di: d){
        int ci = r / di;
        r = r - di * ci;
        *it++ = ci;

        if(r == 0){
            break;
        }
    }
    //Si il reste un reste r, alors l'algo vorace a échoué
    if(r > 0){
        return std::vector<int>{};
    }
    return amounts;
}
