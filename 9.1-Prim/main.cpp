#include <iostream>
#include "makingChange.h"
int main() {
    auto amounts = makeChange(77,std::vector<int>{25,10});
    if(amounts.size() == 0){
        std::cout << "Returned empty vector: greedy algo failed";
    }
    for(auto c: amounts){
        std::cout << c << "\t";
    }
    return 0;
}